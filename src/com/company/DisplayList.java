package com.company;

import java.util.ArrayList;
import java.util.List;

public class DisplayList {

  public static void main(String[] args) {
    List<Integer> lists = new ArrayList<>();
    lists.add(2);
    lists.add(4);
    lists.add(6);
    lists.add(9);
    lists.forEach(System.out::println);
  }

}
