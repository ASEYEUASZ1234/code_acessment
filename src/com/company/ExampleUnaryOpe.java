package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class ExampleUnaryOpe {

  public static void main(String[] args) {
    List<String> dates = new ArrayList<>();
    dates.add("2006/12/01");
    dates.add("2001/09/11");

    UnaryOperator<String> replacedates = date->date.replace("/","-");
    dates.replaceAll(replacedates);
    System.out.println(dates);
    // ce qu'il faut retenir est que le replaceAll est utilisé en faisant d'abord une operation avec
    // l'utilisation de UnaryOperator
  }

}
