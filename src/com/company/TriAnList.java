package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TriAnList  {

  public static void main(String[] args) {
    List<Student> students = new ArrayList<>();
    students.add(new Student("Baba",24));
    students.add(new Student("Gueye",12));
    students.add(new Student("Mamadou",13));

    Collections.sort(students); //because the class student implements Comparable
    System.out.println(students);
  }
  

}
