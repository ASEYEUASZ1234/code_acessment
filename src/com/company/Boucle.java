package com.company;

import java.util.ArrayList;
import java.util.List;

public class Boucle {

  public static void main(String[] args) {
    List<String> list1 = new ArrayList<>();
    list1.add("One");
    list1.add("Two");
    list1.add("Three");

    List<String> list2 = new ArrayList<>();
    list2.add("Two");

    list1.remove(1); // method remove on list return boolean false or true
    System.out.println(list1);
  }

}
