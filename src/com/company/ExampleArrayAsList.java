package com.company;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExampleArrayAsList {
  public static void main(String[] args) {
    //les modifications apportées au niveau de la liste sont directement écrites au niveau du tableau
    String[] array = {"abc", "2", "10", "0"};
    List<String> list = Arrays.asList(array);
    Collections.sort(list);
    System.out.println(Arrays.toString(array));
  }
}
