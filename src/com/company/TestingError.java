package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class TestingError {
  String MESSAGE ="Hello!";
  void print2(){}

  public static void main(String[] args) {
    //ArrayList<String> words = new ArrayList<String>()["Hello", "World"]; not good
    ArrayList<String> words = new ArrayList<>(Arrays.asList("Hello", "World")); // is good
    StringBuilder sb = new StringBuilder("hello");
    //sb.deleteCharAt(0).insert(0, "H")."world"; not working
    sb.deleteCharAt(0).insert(0, "H"); // it possible to insert char on using StringBuilder
    System.out.println(sb);
  }

}
