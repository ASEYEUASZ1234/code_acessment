package com.company;

public class NotOverriding {

    Object message(){
      return "hello ";
    }
    public static void main(String[] args) {
      System.out.print(new NotOverriding().message());
      System.out.print(new NotOverr().message());

  }
}

class NotOverr extends NotOverriding{
  Object message() {
    return "world";
  }
}
