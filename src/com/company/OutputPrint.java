package com.company;

/*
  le String est considéré immuatable car la jvm gère l'optimisation
  du stockage du String en créant une unique copie de chaque chaine au
  niveau du string pool
  à chaque valeur attribuée, il vérifie sur le string pool, si elle est trouvée
  alors la jvm renvoie une référence de l'adresse mémoire
  sinon elle est ajoutée au niveau du string pool et renvoie donc la référence
  dans le cas où c'est créé avec un New une nouvelle espace de stockage est créée
  dans la mémoire Heap (tas)
 */
public class OutputPrint {
  public static void main(String[] args) {
    String message = "Hello"; //same String message="Hello";
    print(message);
    message += "World!"; // ne change pas l'ancienne valeur de String
    print(message);
    message= message.substring(3,5);
    System.out.println("after transform");
    print(message);
  }
  static void print(String message) {
    System.out.print(message);
  }

  /*
    first Hello --> "Hello"
    message = "Hello"+"Hello" + " ";

   */

}
