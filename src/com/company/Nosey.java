package com.company;

public class Nosey {
 static int age; // une variable déclarée dans une classe et utilisée dans dans une méthode statique doit
           // être déclarée static
  public static void main(String[] args) {
    System.out.println("Your age is: " + age);
  }
}
