package com.company;

public class CodeOutput {

  public static void main(String[] args) {
    try {
      System.out.println("Hello World");
    } catch (Exception e) {
      System.out.println("e");
//    } catch (ArithmeticException e) { // not accessible
//      System.out.println("e");
//    } finally {
      System.out.println("!");
    }
  }

}
