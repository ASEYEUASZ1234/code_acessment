package com.company;

public class Permutation {
  public static void main(String[] args) {
    int x=5,y=10;
    swapsies(x,y); //not permutation not transfert by reference
    System.out.println(x+" "+y);
  }

  static void swapsies(int a, int b) {
    int temp=a;
    a=b;
    b=temp;
  }

}
