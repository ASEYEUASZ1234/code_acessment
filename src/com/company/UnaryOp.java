package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class UnaryOp {

  public static void main(String[] args) {
    List<String> dates = new ArrayList<>();
    UnaryOperator<String> replaceSlashes = date -> date.replace("/", "-"); //operation unaire application Function<T,R>
    dates.replaceAll(replaceSlashes);
  }

}
