package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsingPeek {

  public static void main(String[] args) {
    List<String> listes=new ArrayList<>();
    listes.add("ADAMA");
    listes.add("SEYE");
    listes.add("SAMBA");
    listes.add("BOUBACAR");
    listes.add("CAMARA");
    listes.add("TWO");
    listes.add("SI");
    // using consumer forEach
    //listes.forEach(System.out::println);

    //using peek
    // peek fonctionne ici comme une sorte de déboggage en affichant l'ensemble des
    // elements traités au niveau des opérations intermédaires
    // peek ne peut pas etre utilisé sur des opérations de type terminale car elle est utile pour les
    // operations de type intermédiaire.
    listes.stream()
        .filter(e->e.length()>3)
        .peek(e->System.out.println("filtered value "+e))
        .map(e->e.toLowerCase())
        .peek(e->System.out.println("element transformed "+e))
        .collect(Collectors.toList());

  }

}
