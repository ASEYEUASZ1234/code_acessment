package com.company.heritage;

public class Main {

  public static void main(String[] args) {
    Animal dog = new Dog();
    dog.shout();

    Animal cat = new Cat();
    cat.shout();

    System.out.println("verify type Object");
    if(dog instanceof Animal && cat instanceof Animal){
      System.out.println("All is a an Animal.....");
    }

  }

}
