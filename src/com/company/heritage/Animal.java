package com.company.heritage;

/*
 les classes abstraites peuvent avoir des constructeurs mais ne peuvent pas être instanciées
 car elles sont le plus utilisées pour des notions d'héritage
 */
public abstract class Animal {
  protected String name;
  protected String specify;
  protected String description;

  public Animal(String name, String specify, String description) {
    this.name = name;
    this.specify = specify;
    this.description = description;
  }

  public Animal() {
  }

  void shout(){
    System.out.println(" shout animal ");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSpecify() {
    return specify;
  }

  public void setSpecify(String specify) {
    this.specify = specify;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
