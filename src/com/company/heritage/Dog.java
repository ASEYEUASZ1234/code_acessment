package com.company.heritage;

public class Dog extends Animal {

  public Dog(String name, String specify, String description) {
    super(name, specify, description);

  }

  public Dog() {
  }

  @Override
  void shout() {
    super.shout();
    System.out.println("shout dog !!!");
  }
}
