package com.company;

public class ExceptionInstruction {
  public static void main(String[] args) {
    try {
      System.out.println("A"); // first print A and go to throw error on badMethod
      badMethod();
      System.out.println("B");
    } catch (Exception ex) {
      System.out.println("C");
    } finally {
      System.out.println("D");  // second print D for the finally execution
    }
  }
  public static void badMethod() {
    throw new Error();
  }

}
